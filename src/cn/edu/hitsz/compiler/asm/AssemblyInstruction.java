package cn.edu.hitsz.compiler.asm;


/** 汇编指令的字符串表示形式 */
public class AssemblyInstruction {
    private String name;
    private String dst;
    private String rs1;
    private String rs2;

    /**缩进 */
    private String tab = "    ";
    private String annotation = "";

    public AssemblyInstruction(String name, String dst, String rs1, String rs2) {
        this.name = name;
        this.dst = dst;
        this.rs1 = rs1;
        this.rs2 = rs2;
    }

    public AssemblyInstruction(String name, String dst, String rs1) {
        this.name = name;
        this.dst = dst;
        this.rs1 = rs1;
        this.rs2 = "";
    }

    public AssemblyInstruction setAnnotation(String annotation) {
        this.annotation = tab + "# " + annotation;
        return this;
    }

    @Override
    public String toString() {
        String gap = ", ";
        if( rs2.equals("")){
            gap = tab;
        }
        return name + " " + dst + ", " + rs1 + gap + rs2 + annotation +"\n";
    }
}
