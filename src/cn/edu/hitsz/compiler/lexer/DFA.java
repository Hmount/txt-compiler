package cn.edu.hitsz.compiler.lexer;

public class DFA {

    private State cur_state;

    // 初始化状态
    public DFA(State init_state) {
        this.cur_state = init_state;
    }

    // 状态迁移
    public void transition(State next_state){
        this.cur_state = next_state;
    }

    // 获取当前状态
    public State getCur_state() {
        return cur_state;
    }
}
