package cn.edu.hitsz.compiler.lexer;

public enum State {
    START, WORD, NUM;
}
