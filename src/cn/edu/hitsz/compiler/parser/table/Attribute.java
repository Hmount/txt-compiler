package cn.edu.hitsz.compiler.parser.table;


/**
 * 文法符号代表的语义属性
 */
public class Attribute {
    /** 文法符号的值或变量名 **/
    private String name;

    public Attribute(String name) {
        this.name = name;
    }


    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


}
